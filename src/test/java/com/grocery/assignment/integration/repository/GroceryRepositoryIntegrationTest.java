package com.grocery.assignment.integration.repository;

import com.grocery.assignment.entity.Grocery;
import com.grocery.assignment.repository.GroceryRepository;
import com.grocery.assignment.service.IInitialDataLoadService;
import com.grocery.assignment.service.impl.InitialDataLoadServiceImpl;
import com.grocery.assignment.vo.GroceryItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Jpa Data tests
 */
@ExtendWith(SpringExtension.class)
@DataJpaTest
public class GroceryRepositoryIntegrationTest {
    @Autowired
    private GroceryRepository groceryRepository;

    @TestConfiguration
    static class IInitialDataLoadServiceTestConfiguration {
        @Bean
        public IInitialDataLoadService initialDataLoadService() {
            return new InitialDataLoadServiceImpl();
        }
    }

    @Test
    public void testFindByItemNames() {
        List<Grocery> groceryList = groceryRepository.findByItemName("Amla");
        assertThat(groceryList).isNotEmpty();
        assertThat(groceryList).flatExtracting(Grocery::getItemName).contains("Amla");
    }

    @Test
    public void testFindByItemNames_NotPresent() {
        List<Grocery> groceryList = groceryRepository.findByItemName("Apple");
        assertThat(groceryList).isEmpty();
    }

    @Test
    public void testFindAll() {
        Pageable pageable = PageRequest.of(0, 10, Sort.by("itemName").ascending().and(Sort.by("priceDate").ascending()));
        Page<Grocery> groceryList = groceryRepository.findAllExcludingNull(pageable);
        assertThat(groceryList).isNotEmpty();
        assertThat(groceryList).hasSize(10);
    }

    @Test
    public void testFindItemNames() {
        List<String> itemNameList = groceryRepository.findItemNames();
        assertThat(itemNameList).isNotEmpty();
        assertThat(itemNameList).doesNotHaveDuplicates();
    }

    @Test
    public void testGetGroeryItemList() {
        List<GroceryItem> groceryItemList = groceryRepository.getGroceryItemList("Amla");
        assertThat(groceryItemList).isNotEmpty();
    }

}
