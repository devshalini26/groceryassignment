package com.grocery.assignment.integration.controller;

import com.grocery.assignment.GroceryAssignmentApplication;
import com.grocery.assignment.entity.Grocery;
import com.grocery.assignment.repository.GroceryRepository;
import com.grocery.assignment.vo.GroceryItem;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration test for Rst Controller
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = GroceryAssignmentApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.yml")
public class GroceryRestControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;
    @Autowired
    private GroceryRepository repository;

    @Test
    public void testGetAllGrocery() throws Exception {
        mvc.perform(get("/grocery/getAllGrocery?page=0&size=1").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void testGetGroceryNames() throws Exception {
        List<String> names = repository.findItemNames();
        mvc.perform(get("/grocery/getGroceryNames").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(equalTo(names.size()))));
    }

    @Test
    public void testGetGroceryByName() throws Exception {
        List<Grocery> groceryList = repository.findByItemName("Amla");
        mvc.perform(get("/grocery/getGroceryByName?itemName=Amla").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(equalTo(groceryList.size()))))
                .andExpect(jsonPath("$[0].itemName", is("Amla")));
    }

    @Test
    public void testGetGroceryByName_NotFound() throws Exception {
        mvc.perform(get("/grocery/getGroceryByName?itemName=Test").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetItemByNamesForReport() throws Exception {
        List<GroceryItem> groceryItemList = repository.getGroceryItemList("Amla");
        mvc.perform(get("/grocery/getItemByNamesForReport?itemName=Amla").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(equalTo(groceryItemList.size()))));
    }

    @Test
    public void testRandomUrl() throws Exception {
        mvc.perform(get("/grocery/someRandomUrlToTest").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}
