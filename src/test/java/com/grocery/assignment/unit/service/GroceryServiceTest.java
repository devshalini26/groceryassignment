package com.grocery.assignment.unit.service;

import com.grocery.assignment.entity.Grocery;
import com.grocery.assignment.exception.GroceryNotFoundException;
import com.grocery.assignment.repository.GroceryRepository;
import com.grocery.assignment.service.IGroceryService;
import com.grocery.assignment.service.impl.GroceryServiceImpl;
import com.grocery.assignment.vo.GroceryItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

/**
 * Test GroceryService
 */
@ExtendWith(SpringExtension.class)
public class GroceryServiceTest {
    @TestConfiguration
    static class GroceryServiceTestConfiguration {
        @Bean
        public IGroceryService groceryService() {
            return new GroceryServiceImpl();
        }
    }

    @Autowired
    private IGroceryService groceryService;
    @MockBean
    private GroceryRepository repository;

    @BeforeEach
    public void setUp() {
        Grocery grocery1 = new Grocery(1L, 1L, "Apple", LocalDateTime.now(), 40D);
        Grocery grocery2 = new Grocery(2L, 2L, "Orange", LocalDateTime.now(), 48D);
        Grocery grocery3 = new Grocery(3L, 1L, "Apple", LocalDateTime.now(), 48D);
        List<Grocery> groceryList = new ArrayList<>();
        groceryList.add(grocery1);
        groceryList.add(grocery2);
        List<Grocery> groceryList1 = new ArrayList<>();
        groceryList1.add(grocery1);
        groceryList1.add(grocery3);
        Pageable pageable = PageRequest.of(0, 2, Sort.by("itemName").ascending().and(Sort.by("priceDate").ascending()));
        Page<Grocery> page = new PageImpl<>(groceryList.subList(0, 2), pageable, groceryList.size());
        List<String> itemNames = new ArrayList<>();
        itemNames.add("Apple");
        itemNames.add("Orange");
        List<GroceryItem> groceryItemList = new ArrayList<>();
        groceryItemList.add(new GroceryItem(20D, LocalDateTime.now()));

        Mockito.when(repository.findItemNames()).thenReturn(itemNames);
        Mockito.when(repository.findByItemName("wrong_name")).thenReturn(new ArrayList<>());
        Mockito.when(repository.findByItemName("Apple")).thenReturn(groceryList1);
        Mockito.when(repository.getGroceryItemList("Apple")).thenReturn(groceryItemList);
        Mockito.when(repository.findAllExcludingNull(Mockito.any(Pageable.class))).thenReturn(page);
    }

    @Test
    public void testGetSortedGroceryList() {
        Pageable pageable = PageRequest.of(0, 2, Sort.by("itemName").ascending().and(Sort.by("priceDate").ascending()));
        groceryService.getSortedGroceryList(0, 2);
        Mockito.verify(repository, VerificationModeFactory.times(1)).findAllExcludingNull(pageable);
    }

    @Test
    public void testGetGroceryNames() {
        List<String> itemNames = groceryService.getGroceryNames();
        assertThat(itemNames).doesNotHaveDuplicates();
    }

    @Test
    public void testGetGroceryByName() {
        List<Grocery> groceryList = groceryService.getGroceryByName("Apple");
        assertThat(groceryList).hasSize(2).extracting(Grocery::getItemName).contains("Apple");
    }

    @Test
    public void testGetGroceryByNameNotFound() {
        Throwable thrown = catchThrowable(() -> groceryService.getGroceryByName("wrong_name"));
        assertThat(thrown).isInstanceOf(GroceryNotFoundException.class);
    }

    @Test
    public void testGetGroceryItemList() {
        List<GroceryItem> groceryItemList = groceryService.getGroceryItemList("Apple");
        assertThat(groceryItemList).isNotEmpty();
    }

}
