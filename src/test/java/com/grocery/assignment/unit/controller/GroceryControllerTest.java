package com.grocery.assignment.unit.controller;

import com.grocery.assignment.controller.GroceryController;
import com.grocery.assignment.entity.Grocery;
import com.grocery.assignment.exception.GroceryNotFoundException;
import com.grocery.assignment.service.IGroceryService;
import com.grocery.assignment.service.IInitialDataLoadService;
import com.grocery.assignment.vo.GroceryItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.*;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test GroceryController
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(GroceryController.class)
public class GroceryControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private IGroceryService service;
    @MockBean
    private IInitialDataLoadService dataLoadService;

    @Test
    public void testGetAllGrocery() throws Exception {
        List<Grocery> groceryList = new ArrayList<>();
        groceryList.add(new Grocery(1L, 1L, "Apple", LocalDateTime.now(), 40D));
        groceryList.add(new Grocery(2L, 2L, "Orange", LocalDateTime.now(), 48D));
        Pageable pageable = PageRequest.of(0, 2, Sort.by("itemName").ascending().and(Sort.by("priceDate").ascending()));
        Page<Grocery> page = new PageImpl<>(groceryList.subList(0, 2), pageable, groceryList.size());
        when(service.getSortedGroceryList(0, 2)).thenReturn(page);
        mvc.perform(get("/grocery/getAllGrocery?page=0&size=2").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void testGetGroceryNames() throws Exception {
        List<String> itemNames = new ArrayList<>();
        itemNames.add("Apple");
        itemNames.add("Orange");
        when(service.getGroceryNames()).thenReturn(itemNames);
        mvc.perform(get("/grocery/getGroceryNames").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(equalTo(itemNames.size()))));
    }

    @Test
    public void testGetGroceryByName() throws Exception {
        List<Grocery> groceryList = new ArrayList<>();
        groceryList.add(new Grocery(1L, 1L, "Apple", LocalDateTime.now(), 40D));
        groceryList.add(new Grocery(2L, 1L, "Apple", LocalDateTime.now(), 48D));
        when(service.getGroceryByName("Apple")).thenReturn(groceryList);
        mvc.perform(get("/grocery/getGroceryByName?itemName=Apple").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(equalTo(groceryList.size()))))
                .andExpect(jsonPath("$[0].itemName", is("Apple")));
    }

    @Test
    public void testGetGroceryByName_NotFound() throws Exception {
        when(service.getGroceryByName("wrong_name")).thenThrow(new GroceryNotFoundException("Could not find record for wrong_name"));
        mvc.perform(get("/grocery/getGroceryByName?itemName=wrong_name").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetItemByNamesForReport() throws Exception {
        List<GroceryItem> groceryItemList = new ArrayList<>();
        groceryItemList.add(new GroceryItem(20D, LocalDateTime.now()));
        when(service.getGroceryItemList("Apple")).thenReturn(groceryItemList);

        mvc.perform(get("/grocery/getItemByNamesForReport?itemName=Apple").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(equalTo(groceryItemList.size()))));
    }
}
