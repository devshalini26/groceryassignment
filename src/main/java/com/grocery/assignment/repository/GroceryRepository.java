package com.grocery.assignment.repository;

import com.grocery.assignment.entity.Grocery;
import com.grocery.assignment.vo.GroceryItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for Grocery
 */
@Repository
public interface GroceryRepository extends JpaRepository<Grocery, Long> {

    default Page<Grocery> findAllExcludingNull(Pageable pageable) {
        return findAll(pageable);
    }

    @Query("select g from Grocery g where g.itemName is not null and (g.itemName, g.price) = ANY ( select g2.itemName, max(g2.price) from Grocery g2 group by g2.itemName) ")
    Page<Grocery> findAll(Pageable pageable);

    @Query("select distinct g.itemName from Grocery g where g.itemName is not null")
    List<String> findItemNames();

    @Query("select g from Grocery g where g.itemName = :itemName")
    List<Grocery> findByItemName(String itemName);

    @Query("SELECT new com.grocery.assignment.vo.GroceryItem(g.price, g.priceDate) from Grocery g where g.itemName = :itemName")
    List<GroceryItem> getGroceryItemList(String itemName);
}
