package com.grocery.assignment.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Entity bean
 */
@Entity
@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class Grocery {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(name = "ITEM_ID")
    private final Long itemID;
    @Column(name = "ITEM_NAME")
    private final String itemName;
    @Column(name = "PRICE_DATE")
    private final LocalDateTime priceDate;
    @Column(name = "PRICE")
    private final Double price;

    public Grocery() {
        this.itemID = null;
        this.itemName = null;
        this.priceDate = null;
        this.price = null;
    }
}
