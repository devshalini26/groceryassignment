package com.grocery.assignment.controller;

import com.grocery.assignment.entity.Grocery;
import com.grocery.assignment.service.IGroceryService;
import com.grocery.assignment.vo.GroceryItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller for Grocery
 */
@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/grocery")
public class GroceryController {

    @Autowired
    private IGroceryService groceryService;

    @GetMapping(path = "/getAllGrocery", params = {"page", "size"})
    public Page<Grocery> getSortedGroceryList(@RequestParam("page") int page, @RequestParam("size") int size) {
        return groceryService.getSortedGroceryList(page, size);
    }

    @GetMapping(path = "/getGroceryNames")
    public List<String> getGroceryNames() {
        return groceryService.getGroceryNames();
    }

    @GetMapping(path = "/getGroceryByName", params = {"itemName"})
    public List<Grocery> getGroceryByName(@RequestParam("itemName") String itemName) {
        return groceryService.getGroceryByName(itemName);
    }

    @GetMapping(path = "/getItemByNamesForReport", params = {"itemName"})
    public List<GroceryItem> getGroceryItemList(@RequestParam("itemName") String itemName) {
        return groceryService.getGroceryItemList(itemName);
    }
}
