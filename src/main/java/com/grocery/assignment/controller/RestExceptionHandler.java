package com.grocery.assignment.controller;

import com.grocery.assignment.exception.GroceryNotFoundException;
import com.grocery.assignment.exception.ItemNotFoundErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

/**
 * Global Exception Handler
 */
@ControllerAdvice
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(GroceryNotFoundException.class)
    protected ResponseEntity<Object> handleGroceryNotFound(Exception exception, WebRequest request) {
        log.error(exception.getMessage());
        ItemNotFoundErrorResponse errorResponse = new ItemNotFoundErrorResponse();
        errorResponse.setError(exception.getMessage());
        errorResponse.setStatus(HttpStatus.NOT_FOUND.value());
        errorResponse.setTimestamp(LocalDateTime.now());
        return handleExceptionInternal(exception, errorResponse, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

}
