package com.grocery.assignment.service;

/**
 * loads data from xlsx to H2 DB
 */
public interface IInitialDataLoadService {

    /**
     * loads data from xlsx file to db on start-up
     */
    void initialDataLoad();
}
