package com.grocery.assignment.service;

import com.grocery.assignment.entity.Grocery;
import com.grocery.assignment.vo.GroceryItem;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Grocery Service
 */
public interface IGroceryService {
    /**
     * returns page of grocery, sorted by item name - paginated
     * @param page page
     * @param size size
     * @return page of Grocery
     */
    Page<Grocery> getSortedGroceryList(int page, int size);

    /**
     * returns list of item names
     *
     * @return list of item names
     */
    List<String> getGroceryNames();

    /**
     * returns grocery by item name
     * @param itemName item name
     * @return list of grocery
     */
    List<Grocery> getGroceryByName(String itemName);

    /**
     * return item list to generate report
     * @param itemName item name
     * @return list of GroceryItem
     */
    List<GroceryItem> getGroceryItemList(String itemName);
}
