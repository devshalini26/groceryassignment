package com.grocery.assignment.service.impl;

import com.grocery.assignment.entity.Grocery;
import com.grocery.assignment.repository.GroceryRepository;
import com.grocery.assignment.service.IInitialDataLoadService;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Iterator;

/**
 * Loads data to H2 DB at start-up
 */
@Service
@Slf4j
public class InitialDataLoadServiceImpl implements IInitialDataLoadService {
    @Autowired
    private GroceryRepository groceryRepository;

    @Override
    public void initialDataLoad() {
        log.info("Start initial data load to database");
        try {
            DateTimeFormatter dateTimeFormatter = new DateTimeFormatterBuilder().appendPattern("[dd-MM-yyyy][dd/MM/yyyy]").parseDefaulting(ChronoField.HOUR_OF_DAY, 0).parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0).toFormatter();
            IOUtils.setByteArrayMaxOverride(200000000);
            File file = ResourceUtils.getFile("classpath:data/vegetables.xlsx");
            Workbook workbook = new XSSFWorkbook(new FileInputStream(file));
            Sheet sheet = workbook.getSheet("Sheet1");
            Iterator<Row> rowIterator = sheet.rowIterator();
            rowIterator.next();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Long itemId = Double.valueOf(row.getCell(0).getNumericCellValue()).longValue();
                String itemName = row.getCell(1)==null?null:row.getCell(1).getStringCellValue();
                Cell dateCell = row.getCell(2);
                LocalDateTime date = null;
                if (dateCell.getCellType() == CellType.STRING) {
                    date = LocalDateTime.parse(dateCell.getStringCellValue(), dateTimeFormatter);
                } else {
                    date = dateCell.getDateCellValue().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
                }
                Cell priceCell = row.getCell(3);
                Double price = null;
                if (priceCell.getCellType() == CellType.NUMERIC) {
                    price = priceCell.getNumericCellValue();
                }
                Grocery grocery = new Grocery(itemId, itemName, date, price);
                groceryRepository.save(grocery);
            }
        } catch (IOException e) {
            log.error("Error in loading initial data : " + e.getMessage());
        }

        log.info("Initial data load complete");
    }

}
