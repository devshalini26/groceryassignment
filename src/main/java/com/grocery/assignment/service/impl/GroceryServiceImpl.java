package com.grocery.assignment.service.impl;

import com.grocery.assignment.entity.Grocery;
import com.grocery.assignment.exception.GroceryNotFoundException;
import com.grocery.assignment.repository.GroceryRepository;
import com.grocery.assignment.service.IGroceryService;
import com.grocery.assignment.vo.GroceryItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Implementation of IGroceryService
 */
@Service
@Slf4j
public class GroceryServiceImpl implements IGroceryService {

    @Autowired
    private GroceryRepository groceryRepository;

    @Override
    public Page<Grocery> getSortedGroceryList(int page, int size) {
        log.debug("Pagination - page: " + page + ", size: " + size);
        Pageable pageable = PageRequest.of(page, size, Sort.by("itemName").ascending().and(Sort.by("priceDate").ascending()));
        return groceryRepository.findAllExcludingNull(pageable);
    }

    @Override
    public List<String> getGroceryNames() {
        log.info("Fetching list of distinct item names");
        return groceryRepository.findItemNames();
    }

    @Override
    public List<Grocery> getGroceryByName(String itemName) {
        log.info("Fetching grocery details for item name: " + itemName);
        List<Grocery> itemList = groceryRepository.findByItemName(itemName);
        if(itemList.isEmpty()) {
            throw new GroceryNotFoundException("Could not find record for " + itemName);
        }
        log.debug("Fetched list: " + itemList);
        return itemList;
    }

    @Override
    public List<GroceryItem> getGroceryItemList(String itemName) {
        log.info("Fetching price and date for item name: " + itemName);
        return groceryRepository.getGroceryItemList(itemName);
    }


}
