package com.grocery.assignment;

import com.grocery.assignment.service.IInitialDataLoadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GroceryAssignmentApplication implements CommandLineRunner {
	@Autowired
	IInitialDataLoadService initialDataLoadService;

	public static void main(String[] args) {
		SpringApplication.run(GroceryAssignmentApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		initialDataLoadService.initialDataLoad();
	}
}
