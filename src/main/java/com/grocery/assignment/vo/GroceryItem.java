package com.grocery.assignment.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GroceryItem {
    private Double itemPrice;
    private LocalDateTime priceDate;
}
