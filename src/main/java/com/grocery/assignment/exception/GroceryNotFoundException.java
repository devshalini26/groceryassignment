package com.grocery.assignment.exception;

/**
 * Custom runtime exception class
 */
public class GroceryNotFoundException extends RuntimeException {
    public GroceryNotFoundException() {}
    public GroceryNotFoundException(String message) { super(message); }
}
